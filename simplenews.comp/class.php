<?php

use Bitrix\Main\Loader;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Application;

class SimpleNews extends CBitrixComponent
{
    public int $iblockId;
    public int $pageSize;

    const templateTitle = "Список новостей {XX} шт.";
    const navParam = "nav-more-news";

    public function executeComponent()
    {
        global $APPLICATION;

        $this->iblockId = $this->arParams["IBLOCK_ID"];
        if(empty($this->iblockId)) {
            return;
        }
        $this->pageSize = $this->arParams["PAGE_SIZE"] ?? 10;

        Loader::IncludeModule("iblock");

        $years = $this->getYears();
        $pickedYear = $this->request->get("year") ?? $this->getClosestYear($years,date("Y"));
        if(!in_array($pickedYear,$years)) {
            return;
        }

        $this->arResult["PICKED_YEAR"] = $pickedYear;
        $this->arResult["YEARS"] = $years;

        //Кэширование
        $cache = Application::getInstance()->getManagedCache();
        $navParam = $_GET[self::navParam];
        $cacheId = "{$pickedYear}_{$navParam}";
        if ($cache->read($this->arParams["CACHE_TIME"], $cacheId)) {
            $arr = $cache->get($cacheId);
            $this->arResult["NEWS"] = $arr["news"];
            $this->arResult["TITLE"] = $arr["title"];
            $this->arResult["NAV"] = $arr["nav"];
        } else {
            $news = $this->getNews($pickedYear);
            $this->arResult["NEWS"] = $news;
            $cache->set($cacheId, [
                "news"=>$this->arResult["NEWS"],
                "title" => $this->arResult["TITLE"],
                "nav" => $this->arResult["NAV"]
            ]);
        }
        $APPLICATION->setTitle($this->arResult["TITLE"]);
        $this->includeComponentTemplate();
    }

    /**
     * Получение таба годов
     * @return array
     */
    public function getYears()
    {
        $res = \Bitrix\Iblock\ElementTable::getList(array(
            'order' => array('ACTIVE_FROM' => 'DESC'),
            'select' => array('ACTIVE_FROM'),
            'filter' => array('IBLOCK_ID' => $this->iblockId,"ACTIVE"=>"Y"),
            'cache' => array(
                'ttl' => 60,
                'cache_joins' => true,
            )
        ));
        $years = [];
        while ($item = $res->fetch()) {
            if(!empty($item["ACTIVE_FROM"])) {
                $year = $item["ACTIVE_FROM"]->format("Y");
                $years[$year] = $year;
            }
        }

        return $years;
    }

    /**
     * Получение новостей по году
     * @param $year
     * @return array
     */
    public function getNews($year)
    {
        $nav = new PageNavigation(self::navParam);
        $nav->allowAllRecords(true)
            ->setPageSize($this->pageSize)
            ->initFromUri();

        $res = ElementTable::getList(array(
            'order' => array('SORT' => 'ASC'),
            'select' => array('ID', 'NAME', 'ACTIVE_FROM', 'PREVIEW_TEXT', 'PREVIEW_PICTURE'),
            'filter' => array(
                'IBLOCK_ID' => $this->iblockId,
                "ACTIVE" => "Y",
                ">=ACTIVE_FROM" => "01.01.{$year} 00:00:00",
                "<=ACTIVE_FROM" => "31.12.{$year} 23:59:59"
            ),
            "count_total" => true,
            "offset" => $nav->getOffset() ?? 0,
            "limit" => $nav->getLimit() ?? 9999,
            'cache' => array(
                'ttl' => 60,
                'cache_joins' => true,
            )
        ));

        $nav->setRecordCount($res->getCount());
        $this->arResult["NAV"] = $nav;
        $this->arResult["TITLE"] = str_replace("{XX}",$res->getCount(),self::templateTitle);

        $news = [];
        while ($item = $res->fetch()) {
            $item["PREVIEW_PICTURE_PATH"] = CFile::GetPath($item["PREVIEW_PICTURE"]);
            if(!empty($item["ACTIVE_FROM"])) {
                $item["ACTIVE_FROM_DATE"] = $item["ACTIVE_FROM"]->format("d.m.Y H:i:s");
            }
            $news[] = $item;
        }

        return $news;
    }

    /**
     * Получить ближайший год к выбранному году
     * @param $years
     * @param $year
     * @return mixed
     */
    public function getClosestYear($years,$year)
    {
        return array_reduce($years, function ($carry, $item) use ($year) {
            return $item <= $year? max($carry, $item): $carry;
        });
    }
}