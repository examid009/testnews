<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
?>
<div>
    <?php foreach ($arResult["YEARS"] as $year):?>
        <a href="<?=$APPLICATION->GetCurPage(false)."?year={$year}"?>"
        <?php if($arResult["PICKED_YEAR"]==$year):?>style="color:red"<?endif;?>
        ><?=$year?></a>
    <?php endforeach;?>
</div>
<br>
<div>
    <?php foreach ($arResult["NEWS"] as $news):?>
        <div>
            <b><?=$news["NAME"]?></b> - <span><?=$news["ACTIVE_FROM_DATE"]?></span>
        </div>
        <div>
            <img src="<?=$news['PREVIEW_PICTURE_PATH']?>" width="100px" height="100px">
        </div>
        <div>
            <?=$news["PREVIEW_TEXT"]?>
        </div>
    <?php endforeach;?>
</div>
<br>
<?php
$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "",
    array(
        "NAV_OBJECT" => $arResult["NAV"],
        "SEF_MODE" => "N",
    ),
    false
);
?>
