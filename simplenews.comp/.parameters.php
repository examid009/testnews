<?php
$arComponentParameters = array(
    "PARAMETERS" => array(
        "IBLOCK_ID" => array(
            "NAME" => "ID инфоблока",
            "TYPE" => "STRING",
        ),
        "PAGE_SIZE" => array(
            "NAME" => "Элементов на страницу",
            "TYPE" => "STRING",
        ),
        "CACHE_TIME" => array(),
    )
);
?>